package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Users;
import com.binar.TikMovApps.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void saveUser(String username, String password, String email) {
        Users user = new Users();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        usersRepository.save(user);
    }

    @Override
    public List<Users> getUsersByUserId(Integer userId) {
        return usersRepository.findUsersByUserId(userId);
    }

    @Override
    public void updateUser(String username, String email, String password, Integer userId) {
        usersRepository.updateUser(username, email, password, userId);
    }

    @Override
    public void deleteUsers(Integer userId) {
        usersRepository.deleteUsersByUserId(userId);
    }

    @Override
    public List<Users> userList() {
        return usersRepository.findAll();
    }

    @Override
    public Users getByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

}
