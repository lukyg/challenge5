package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Seats;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SeatService {
    public List<Seats> getByNamaStudio(String namaStudio);
}
