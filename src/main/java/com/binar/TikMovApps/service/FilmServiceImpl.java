package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Schedules;
import com.binar.TikMovApps.model.Seats;
import com.binar.TikMovApps.repository.FilmRepository;
import com.binar.TikMovApps.repository.ScheduleRepository;
import com.binar.TikMovApps.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmServiceImpl implements FilmService, ScheduleService, SeatService{

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private SeatRepository seatRepository;

    @Override
    public void saveFilm(String filmCode, String namaFilm, String statusTayang){
        Films films = new Films();
        films.setFilmCode(filmCode);
        films.setNamaFilm(namaFilm);
        films.setStatusTayang(statusTayang);
        filmRepository.save(films);
    }

    @Override
    public void updateFilm(String filmCode, String namaFilm, String statusTayang, Integer filmId){
        filmRepository.updateFilm(filmCode, namaFilm, statusTayang, filmId);
    }

    @Override
    public void deleteFilm(String namaFilm){
        filmRepository.deleteFilm(namaFilm);
    }

    @Override
    public List<Films> findFilmsByNamaFilm(String namaFilm) {
        return filmRepository.findFilmsByNamaFilm(namaFilm);
    }

    @Override
    public List<Films> filmsList(){
        return filmRepository.findAll();
    }

    @Override
    public List<Schedules> getByNamaFilm(String namaFilm) {
        return scheduleRepository.findByNamaFilm(namaFilm);
    }

    @Override
    public List<Schedules> schedulesList() {
        return scheduleRepository.findAll();
    }

    @Override
    public List<Seats> getByNamaStudio(String namaStudio) {
        return seatRepository.findByNamaStudio(namaStudio);
    }
}
