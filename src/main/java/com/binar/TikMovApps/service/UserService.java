package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface UserService {
    public void saveUser(String username, String password, String email);
    public List<Users> getUsersByUserId (Integer userId);
    public void updateUser(String username, String email, String password, Integer userId);
    public void deleteUsers(Integer userId);
    public List<Users> userList();
    public Users getByUsername(String username);
}
