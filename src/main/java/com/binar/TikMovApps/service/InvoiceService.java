package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Schedules;
import com.binar.TikMovApps.model.Seats;
import com.binar.TikMovApps.model.Users;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InvoiceService {

    @Autowired
    private UserService userService;

    @Autowired
    private FilmService filmService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private SeatService seatService;

    private JasperPrint getJasperPrint(List<Map<String, Object>> userCollection, String resourceLocation) throws FileNotFoundException, JRException {
        File file = ResourceUtils.getFile(resourceLocation);
        JasperReport jasperReport = JasperCompileManager
                .compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new
                JRBeanCollectionDataSource(userCollection);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy","Ade Madani");

        JasperPrint jasperPrint = JasperFillManager
                .fillReport(jasperReport,parameters,dataSource);

        return jasperPrint;
    }

    private Path getUploadPath(String fileFormat, JasperPrint jasperPrint, String fileName) throws IOException, JRException {
        String uploadDir = StringUtils.cleanPath("src/main/resources");
        Path uploadPath = Paths.get(uploadDir);
        if (!Files.exists(uploadPath)){
            Files.createDirectories(uploadPath);
        }
        //generate the report and save it in the just created folder
        if (fileFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(
                    jasperPrint, uploadPath+fileName
            );
        }

        return uploadPath;
    }

    private String getPdfFileLink(String uploadPath){
        return uploadPath+"/"+"users.pdf";
    }

    public String generateReport(LocalDate localDate, String fileFormat){
        try {
            List<Map<String, Object>> dataList = new ArrayList<>();

            Map<String, Object> data = new HashMap<>();
            Users users = userService.getUsersByUserId(1).get(0);
            data.put("username", users.getUsername());
            Films films = filmService.findFilmsByNamaFilm("Nothingness").get(0);
            data.put("namaFilm",films.getNamaFilm());
            Schedules schedules = scheduleService.schedulesList().get(0);
            data.put("hargaTiket", schedules.getHargaTiket().toString());
            data.put("jamMulai", schedules.getJamMulai());
            data.put("jamSelesai", schedules.getJamSelesai());
            data.put("tanggalTayang", schedules.getTanggalTayang());
            Seats seats = seatService.getByNamaStudio("A").get(0);
            data.put("namaStudio", seats.getNamaStudio());
            data.put("nomorKursi", seats.getNomorKursi());
            dataList.add(data);
            //load the file and compile it
            String resourceLocation = "classpath:Tiket.jrxml";
            JasperPrint jasperPrint = getJasperPrint(dataList,resourceLocation);
            //create a folder to store the report
            String fileName = "/"+"InvoiceTiket.pdf";
            Path uploadPath = getUploadPath(fileFormat, jasperPrint, fileName);
            return getPdfFileLink(uploadPath.toString());
        } catch(Exception e) {
            e.printStackTrace();
        }

        //create a private method that returns the link to the specific pdf file
        return null;
    }

    //    @Override
    public List<Films> findAllProducts() {
        return filmService.filmsList();
    }

}
