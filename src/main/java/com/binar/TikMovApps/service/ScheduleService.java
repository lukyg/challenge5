package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Schedules;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScheduleService {
    public List<Schedules> getByNamaFilm(String namaFilm);
    public List<Schedules> schedulesList();
}
