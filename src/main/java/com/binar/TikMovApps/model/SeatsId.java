package com.binar.TikMovApps.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.io.Serializable;

@Setter
@Getter
public class SeatsId implements Serializable {

    @Column(length = 1)
    private String namaStudio;

    @Column(length = 2)
    private String nomorKursi;

}
