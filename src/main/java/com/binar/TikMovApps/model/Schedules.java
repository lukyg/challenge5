package com.binar.TikMovApps.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity (name = "schedules")
public class Schedules implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "schedules_id")
    private Integer schedulesId;

    @Column(name = "tanggal_tayang")
    private String tanggalTayang;

    @Column(name = "jam_mulai")
    private String jamMulai;

    @Column(name = "jam_selesai")
    private String jamSelesai;

    @Column(name = "harga_tiket")
    private Integer hargaTiket;

    @ManyToOne
    @JoinColumn(name = "film_id")
    private Films filmId;

}
