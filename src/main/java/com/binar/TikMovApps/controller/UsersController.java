package com.binar.TikMovApps.controller;

import com.binar.TikMovApps.model.Users;
import com.binar.TikMovApps.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name = "Users", description = "API for processing certain things with Users entity")
@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "211", description = "Success added a new user")
    })
    @Operation (summary = "Add a new user")
    @PostMapping("/public/add-user")
    public ResponseEntity addUser(@Schema(example = "{" +
            "\"username\":\"lukyg\"," +
            "\"email\":\"palpale@gmail.com\"," +
            "\"password\":\"kukangpemalas\"" +
            "}")
                                     @RequestBody Map<String, Object> user){
        userService.saveUser(user.get("username").toString(), user.get("email").
                toString(), user.get("password").toString());

        Map<String, Object> responBody = new HashMap<>();
        responBody.put("username",user.get("username"));
        responBody.put("email",user.get("email"));
        responBody.put("password",user.get("password"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("Test"));
        return new ResponseEntity(responBody, headers, 211);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "202", description = "Success show all user (ById)")
    })
    @Operation(summary = "Show user by userId")
    @GetMapping("/get-user-by-user-id/{userId}")
    public ResponseEntity getUserByUserId(@PathVariable("userId") Integer userId){
        List<Users> users = userService.getUsersByUserId(userId);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "213", description = "Success updated an user")
    })
    @Operation (summary = "Updates an existing user")
    @PutMapping("/customer/update-user")
    public ResponseEntity updateUser(@Schema(example = "{" +
            "\"username\":\"lukyg\"," +
            "\"email\":\"palpale@gmail.com\"," +
            "\"password\":\"kukangpemalas\"," +
            "\"userId\":\"3\"" +
            "}")
                                     @RequestBody Map<String, Object> user){
        userService.updateUser(user.get("username").toString(), user.get("email").
                toString(), user.get("password").toString(), Integer.valueOf(user.get("userId").toString()));

        Map<String, Object> responBody = new HashMap<>();
        responBody.put("username",user.get("username"));
        responBody.put("email",user.get("email"));
        responBody.put("password",user.get("password"));
        responBody.put("userId",user.get("userId"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("Test"));
        return new ResponseEntity(responBody, headers, 213);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "202", description = "Success deleted a film")
    })
    @Operation(summary = "Deletes an user")
    @DeleteMapping("/customer/delete-user/{userId}")
    public ResponseEntity deleteUser(@PathVariable ("userId") Integer userId){
        userService.deleteUsers(userId);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "202", description = "Success show all registered users")
    })
    @Operation(summary = "Show all registered users")
    @GetMapping(value = "/all-users")
    public ResponseEntity allUsers(){
        List<Users> usersList = userService.userList();
        System.out.println("userId\tusername\t\temail\t\t\t\tpassword");
        usersList.forEach(users -> {
            System.out.println(users.getUserId()+"\t\t"+users.getUsername()+
                    "\t\t"+users.getEmail()+"\t"+users.getPassword());
        });
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
