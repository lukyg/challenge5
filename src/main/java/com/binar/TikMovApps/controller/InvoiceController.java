package com.binar.TikMovApps.controller;

import com.binar.TikMovApps.model.*;
import com.binar.TikMovApps.service.FilmService;
import com.binar.TikMovApps.service.ScheduleService;
import com.binar.TikMovApps.service.SeatService;
import com.binar.TikMovApps.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name = "Invoice", description = "API to process invoices in pdf format")
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    public FilmService filmService;

    @Autowired
    public ScheduleService scheduleService;

    @Autowired
    public UserService userService;

    @Autowired
    public SeatService seatService;

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "202", description = "Success generated ticket invoice")
    })
    @Operation(summary = "Print invoice ticket that required Users, Films, Schedules and Seats Entity")
    @GetMapping("/customer/print-invoice")
    public ResponseEntity getInvoice(HttpServletResponse response) throws IOException, JRException{
        JasperReport sourceFileName = JasperCompileManager
                .compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX
                        + "Tiket.jrxml").getAbsolutePath());

//      creating our list of beans
        List<Map<String, String>> dataList = new ArrayList<>();

        Map<String, String> data = new HashMap<>();
        Users users = userService.getUsersByUserId(6).get(0);
        data.put("username", users.getUsername());
        Films films = filmService.findFilmsByNamaFilm("Putri").get(0);
        data.put("namaFilm",films.getNamaFilm());
        Schedules schedules = scheduleService.schedulesList().get(0);
        data.put("hargaTiket", schedules.getHargaTiket().toString());
        data.put("jamMulai", schedules.getJamMulai());
        data.put("jamSelesai", schedules.getJamSelesai());
        data.put("tanggalTayang", schedules.getTanggalTayang());
        Seats seats = seatService.getByNamaStudio("A").get(0);
        data.put("namaStudio", seats.getNamaStudio());
        data.put("nomorKursi", seats.getNomorKursi());
        dataList.add(data);

//      creating datasource from bean list
        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
        Map<String, Object> parameters = new HashMap();
        parameters.put("createdBy", "lukyg");
        JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, beanColDataSource);

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; filename=invoice.pdf;");
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
