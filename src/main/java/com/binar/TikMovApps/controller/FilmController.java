package com.binar.TikMovApps.controller;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Schedules;
import com.binar.TikMovApps.service.FilmService;
import com.binar.TikMovApps.service.ScheduleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.ResponseEntity.status;

@Tag(name = "Films", description = "API for processing various operations with Users entity")
@RestController
@RequestMapping("/films")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private ScheduleService scheduleService;

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "221", description = "Success added a new film")
    })
    @Operation (summary = "Add a new film to the cinema")
    @PostMapping("/admin/add-film")
    public ResponseEntity addFilm(@Schema(example = "{" +
            "\"filmCode\":\"F777\"," +
            "\"namaFilm\":\"Miss My Home\"," +
            "\"statusTayang\":\"Sedang Tayang\"" +
            "}")
                              @RequestBody Map<String, Object> film){
        filmService.saveFilm(film.get("filmCode").toString(), film.get("namaFilm").toString(), film.get("statusTayang").toString());

        Map<String, Object> responBody = new HashMap<>();
        responBody.put("filmCode",film.get("filmCode"));
        responBody.put("namaFilm",film.get("namaFilm"));
        responBody.put("statusTayang",film.get("statusTayang"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("Test"));
        return new ResponseEntity(responBody, headers, 221);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "222", description = "Success update a film")
    })
    @Operation (summary = "Updates an existing film")
    @PutMapping("/admin/update-film")
    public ResponseEntity updateFilm(@Schema(example = "{" +
            "\"filmCode\":\"F665\"," +
            "\"namaFilm\":\"Take Me Home\"," +
            "\"statusTayang\":\"Tidaktayang\"," +
            "\"filmId\":\"3\"" +
            "}")
                              @RequestBody Map<String, Object> film){
        filmService.updateFilm(film.get("filmCode").toString(), film.get("namaFilm").
                toString(), film.get("statusTayang").toString(), Integer.valueOf(film.get("filmId").toString()));

        Map<String, Object> responBody = new HashMap<>();
        responBody.put("filmCode",film.get("filmCode"));
        responBody.put("namaFilm",film.get("namaFilm"));
        responBody.put("statusTayang",film.get("statusTayang"));
        responBody.put("filmId",film.get("filmId"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("Test"));
        return new ResponseEntity(responBody, headers, 222);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "202", description = "Success deleted a film")
    })
    @Operation (summary = "Deletes a film")
    @DeleteMapping("/admin/delete-film/{namaFilm}")
    public ResponseEntity deleteFilm(@PathVariable ("namaFilm") String namaFilm){
        filmService.deleteFilm(namaFilm);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "202", description = "Success show all available films")
    })
    @Operation (summary = "Show all available films")
    @GetMapping("/public/all-available-film")
    public ResponseEntity allFilm(){
        List<Films> filmsList = filmService.filmsList();
        System.out.println("Film ID\t\tCode Film\t\tNama Film\t\tStatus");
        filmsList.forEach(films -> {
            if(films.getStatusTayang().equalsIgnoreCase("Sedang tayang")){
                System.out.println(films.getFilmId()+"\t\t\t"+films.getFilmCode()+"\t\t\t"+films.getNamaFilm()+
                        "\t\t\t"+films.getStatusTayang());
            }
        });
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "202", description = "Success show all available films")
    })
    @Operation (summary = "Show schedules on certain film")
    @GetMapping(value = "/public/get-by-nama-film/{namaFilm}")
    public ResponseEntity getByNamaFilm(@PathVariable("namaFilm") String namaFilm) {
        List<Schedules> films = scheduleService.getByNamaFilm(namaFilm);
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        films.forEach(flm-> {
            System.out.println(flm.getFilmId().getNamaFilm()+"\t\t\t"+flm.getTanggalTayang()+"\t\t\t"+flm.getJamMulai()+
                    "\t\t\t"+flm.getJamSelesai()+"\t\t\tRp. "+flm.getHargaTiket());
        });
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

}
