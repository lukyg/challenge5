package com.binar.TikMovApps.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    @Bean
    public OpenAPI customOpenAPI(@Value("TikMovApps API is an API that can perform various operations to manage a cinema. " +
            "The TikMovApps API is still under development so if you find any bugs, please report them at the contact below. " +
            "Your support means a lot to us. ")String appDescription,
                                 @Value("v.0.0.1") String appVersion){
        return new OpenAPI().info(
                new Info().title("TikMovApps")
                        .version(appVersion)
                        .description(appDescription)
                        .contact(new Contact()
                                .email("adesfrdnmdn@gmail.com"))
                        .license(new License()
                                .name("Apache 2.0")
                                .url("http://springdocs.org"))
        );
    }
}
